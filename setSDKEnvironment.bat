@echo off

set sdk_dir_=%cd%\

setx ExtraSDK %sdk_dir_%\
setx QTDIR %sdk_dir_%Qt\5.4.1\5.4\msvc2013\
setx QTINCLUDE %sdk_dir_%Qt\5.4.1.\5.4\msvc2013\include\
setx QTBIN %sdk_dir_%Qt\5.4.1\5.4\msvc2013\bin\
setx QTLIB %sdk_dir_%Qt\5.4.1\5.4\msvc2013\lib\
setx FREETYPE %sdk_dir_%freetype-2.4.9\
setx FTGL %sdk_dir_%ftgl\ftgl-2.1.3~rc5\
setx QT_QPA_PLATFORM_PLUGIN_PATH %sdk_dir%Qt\5.4.1\5.4\msvc2013\plugins\platforms\

setx PATH_backup %PATH%
setx PATH %sdk_dir_%Qt\5.4.1\5.4\msvc2013\bin\;%PATH% /m
